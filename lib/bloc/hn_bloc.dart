import 'dart:async';
import 'dart:collection';

import 'package:hn_app/utils/article.dart';
import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

enum StoriesType { topStories, newStories }

class HackerNewsBloc {
  static List<int> _newIds = [
    19411336,
    19397817,
    19400805,
    19401079,
    19411998,
    19409367,
  ];

  static List<int> _topIds = [
    19411731,
    19410233,
    19408731,
    19411927,
    19412088,
    19411732,
  ];

  final _articlesSubject = BehaviorSubject<UnmodifiableListView<Article>>();
  final _storiesTypeController = StreamController<StoriesType>();

  dynamic _articles = <Article>[];

  HackerNewsBloc() {
    _getArticlesAndUpdate(_newIds);

    _storiesTypeController.stream.listen((storiesType) {
      if (storiesType == StoriesType.newStories) {
        _getArticlesAndUpdate(_newIds);
      } else {
        _getArticlesAndUpdate(_topIds);
      }
    });
  }

  Stream<UnmodifiableListView<Article>> get articles => _articlesSubject.stream;

  Sink<StoriesType> get storiesType => _storiesTypeController.sink;

  Future<Null> _updateArticles(List<int> ids) async {
    final futureArticles = ids.map((id) => _fetchArticles(id));
    final articles = await Future.wait(futureArticles);
    _articles = articles;
  }

  _getArticlesAndUpdate(List<int> ids) {
    _updateArticles(ids).then((_) {
      _articlesSubject.add(UnmodifiableListView(_articles));
    });
  }

  Future<Article> _fetchArticles(id) async {
    final url = 'https://hacker-news.firebaseio.com/v0/item/$id.json';
    final res = await http.get(url);
    if (res.statusCode == 200) {
      return parseArticle(res.body);
    }

    return null;
  }
}
