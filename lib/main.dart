import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:hn_app/bloc/hn_bloc.dart';
import 'package:hn_app/utils/article.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  final hnBloc = HackerNewsBloc();
  runApp(MyApp(bloc: hnBloc));
}

class MyApp extends StatelessWidget {
  final HackerNewsBloc bloc;

  MyApp({Key key, this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'HackerNews', bloc: bloc),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.bloc}) : super(key: key);

  final String title;
  final HackerNewsBloc bloc;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int navIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: StreamBuilder<UnmodifiableListView<Article>>(
        stream: widget.bloc.articles,
        initialData: UnmodifiableListView<Article>([]),
        builder: (context, snapshot) => ListView(
              children: snapshot.data.map(_buildItem).toList(),
            ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: navIndex,
        items: [
          BottomNavigationBarItem(
            title: Text('New Stories'),
            icon: Icon(
              Icons.arrow_drop_up,
            ),
          ),
          BottomNavigationBarItem(
            title: Text('Top Stories'),
            icon: Icon(
              Icons.new_releases,
            ),
          ),
        ],
        onTap: (index) {
          if (index == 0) {
            widget.bloc.storiesType.add(StoriesType.newStories);
            setState(() {
              navIndex = 0;
            });
          } else {
            widget.bloc.storiesType.add(StoriesType.topStories);
            setState(() {
              navIndex = 1;
            });
          }
        },
      ),
    );
  }
}

Widget _buildItem(article) {
  return Padding(
    padding: const EdgeInsets.all(25.0),
    child: ExpansionTile(
      key: Key(article.title),
      title: Text(
        article.title,
        style: TextStyle(fontSize: 24.0),
      ),
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              article.type,
              style: TextStyle(fontSize: 20.0),
            ),
            IconButton(
              onPressed: () async {
                if (await canLaunch(article.url)) {
                  launch(article.url);
                }
              },
              icon: Icon(Icons.launch),
            ),
          ],
        ),
      ],
    ),
  );
}
