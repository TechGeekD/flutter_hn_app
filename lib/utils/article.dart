import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:hn_app/utils/serializers.dart';

part 'article.g.dart';


abstract class Article implements Built<Article, ArticleBuilder> {
  static Serializer<Article> get serializer => _$articleSerializer;

  int get id;

  @nullable
  bool get deleted;

  String get type;

  String get by;

  int get time;

  @nullable
  String get text;

  @nullable
  bool get dead;

  @nullable
  int get parent;

  @nullable
  int get poll;

  BuiltList<int> get kids;

  @nullable
  String get url;

  @nullable
  int get score;

  @nullable
  String get title;

  BuiltList<int> get parts;

  @nullable
  int get descendants;

  Article._();

  factory Article([updates(ArticleBuilder b)]) = _$Article;
}

//class Article {
//  final String text;
//  final String url;
//  final String by;
//  final String time;
//  final int score;
//
//  const Article({
//    this.text,
//    this.url,
//    this.by,
//    this.time,
//    this.score,
//  });
//
//  factory Article.fromJSON(Map<String, dynamic> json) {
//    return Article(
//      text: json['text'] ?? '[null]',
//      url: json['url'],
//      by: json['by'],
//      score: json['score'],
//    );
//  }
//}

List<int> parseStoriesId(jsonString) {
  final parsed = jsonDecode(jsonString);
  final listOfIds = List<int>.from(parsed);

  return listOfIds;
}

Article parseArticle(jsonString) {
  final parsed = jsonDecode(jsonString);
  Article article =
  standardSerializers.deserializeWith(Article.serializer, parsed);
  return article;
//  return null;
}
